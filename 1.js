//Для ES6 характерно использование let и const, которые имеют блочную видимость функции,
// поэтому могут обьявлять переменные в любой части кода.
// Переменную let мы можем менять сколько угодно раз, а переменную const только один раз.

// В ES5 используют var, где есть только видимость уровня функции,
// а также обьявление переменной происходит только в начале кода.

// Использование let и const позволяет сделать код более "строгим".
// Var же актуален только при работе с устаревшими браузерами.

let userName = prompt('enter your name');
let userAge = +prompt('enter your age');

while (userName === null || userName.length === 0 || !Number.isNaN(+userName)) {
  userName = prompt('Enter your name');
}
while (userAge === null || userAge.length === 0 || Number.isNaN(userAge)) {
  userAge = prompt('Enter your age');
}

if (userAge < 18) {
  alert('you are not allowed to this website!');
} else if (userAge >= 18 && userAge <= 22) {
  const isContinue = confirm('Are u sure u want to continue');
  if (isContinue) {
    alert(`Welcome ${userName}`);
  } else {
    alert('you r not allowed visit this website');
  }
} else {
  alert(`Welcome ${userName}`);
}